#Power House Use Case solution

## Start application
Checkout repository into folder, and then run

    ./mvnw spring-boot:run        #for Linux based systems
    mvnw.cmd spring-boot:run      #for Windows

Now application available by url [http://localhost:8080](http://localhost:8080)

## Documentation
After Application start simple User guide will be available by url [http://localhost:8080/readme.html](http://localhost:8080/readme.html)