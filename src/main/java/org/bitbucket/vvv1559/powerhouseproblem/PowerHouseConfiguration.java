package org.bitbucket.vvv1559.powerhouseproblem;

import org.bitbucket.vvv1559.powerhouseproblem.dao.CrudYearDao;
import org.bitbucket.vvv1559.powerhouseproblem.dao.model.MeterData;
import org.bitbucket.vvv1559.powerhouseproblem.dao.model.Profile;
import org.bitbucket.vvv1559.powerhouseproblem.dao.validation.ValidationService;
import org.bitbucket.vvv1559.powerhouseproblem.loaders.LegacyFileLoader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:power-house.properties")
public class PowerHouseConfiguration {

    @Bean
    CrudYearDao<String, Profile> profileDao() {
        return new CrudYearDao<>();
    }

    @Bean
    CrudYearDao<Integer, MeterData> meterDataDao() {
        return new CrudYearDao<>();
    }

    @Bean(initMethod = "start")
    public LegacyFileLoader fileLoader() {
        return new LegacyFileLoader(profileDao(), meterDataDao(), validationService());
    }

    @Bean
    public ValidationService validationService() {
        return new ValidationService(profileDao());
    }

}
