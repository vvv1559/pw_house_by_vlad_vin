package org.bitbucket.vvv1559.powerhouseproblem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PowerHouseProblemApplication {

    public static void main(String[] args) {
        SpringApplication.run(PowerHouseProblemApplication.class, args);
    }
}
