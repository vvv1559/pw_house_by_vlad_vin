package org.bitbucket.vvv1559.powerhouseproblem.dao.validation;

import org.bitbucket.vvv1559.powerhouseproblem.dao.CrudYearDao;
import org.bitbucket.vvv1559.powerhouseproblem.dao.model.MeterData;
import org.bitbucket.vvv1559.powerhouseproblem.dao.model.Month;
import org.bitbucket.vvv1559.powerhouseproblem.dao.model.Profile;
import org.springframework.beans.factory.annotation.Autowired;

public class ValidationService {
    private static final double CORRECT_FRACTIONS_SUM = 1.0; // 100%
    private static final double DATA_TOLERANCE_FRACTION = 0.25; // 25%

    private final CrudYearDao<String, Profile> profileDao;

    @Autowired
    public ValidationService(CrudYearDao<String, Profile> profileDao) {
        this.profileDao = profileDao;
    }

    public ValidationResult validateProfile(Profile profile) {
        double fractionsSum = profile.getFractionsSum();

        ValidationResult validationResult = new ValidationResult();
        if (fractionsSum != CORRECT_FRACTIONS_SUM) {
            validationResult.addError("Incorrect fractions sum (%s != %s) for profile %s",
                fractionsSum,
                CORRECT_FRACTIONS_SUM,
                profile.getName());
        }

        return validationResult;
    }

    public ValidationResult validateMeterData(MeterData meterData) {
        ValidationResult validationResult = new ValidationResult();

        Profile profile = profileDao.getData(meterData.getProfile());
        if (profile == null) {
            validationResult.addError("Profile %s not found", meterData.getProfile());
        }

        Month[] months = Month.values();
        int[] consumptions = new int[months.length];
        consumptions[0] = meterData.getReading(Month.JAN);
        int consumptionSum = 0;
        for (int i = 1; i < months.length; i++) {
            int consumption = meterData.getReading(months[i]) - meterData.getReading(months[i - 1]);
            consumptions[i] = consumption;
            consumptionSum += consumption;
        }

        for (int i = 1; i < consumptions.length; i++) {
            int consumption = consumptions[i];
            Month currentMonth = months[i];
            if (consumption < 0) {
                String errorFormat = "Negative consumption for month %s = %d";
                validationResult.addError(errorFormat, currentMonth, consumption);
            } else if (profile != null) {
                double monthFraction = profile.getFraction(currentMonth);
                int expectedConsumption = (int) (consumptionSum * monthFraction);
                int maxConsumption = (int) (expectedConsumption * (1 + DATA_TOLERANCE_FRACTION));
                int minConsumption = (int) (expectedConsumption * (1 - DATA_TOLERANCE_FRACTION));
                if (consumption < minConsumption || consumption > maxConsumption) {
                    validationResult.addError("Consumption for month %s (fraction %s ) not in expected range [%d;%d]",
                        currentMonth, monthFraction, minConsumption, maxConsumption
                    );
                }
            }
        }

        return validationResult;
    }
}
