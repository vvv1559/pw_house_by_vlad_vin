package org.bitbucket.vvv1559.powerhouseproblem.dao.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.bitbucket.vvv1559.powerhouseproblem.Utils;

import java.util.Map;
import java.util.Objects;

public class Profile {
    private final String name;

    @JsonSerialize
    private final Map<Month, Double> fractions = Utils.initMonthMap();

    public Profile(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setFraction(Month month, double fraction) {
        fractions.put(month, fraction);
    }

    public double getFraction(Month month) {
        return fractions.get(month);
    }

    @JsonIgnore
    public double getFractionsSum() {
        double fractionsSum = 0;
        for (Month month : Month.values()) {
            fractionsSum += fractions.get(month);
        }

        return fractionsSum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Profile profile = (Profile) o;
        return Objects.equals(name, profile.name) &&
            Objects.equals(fractions, profile.fractions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, fractions);
    }

    @Override
    public String toString() {
        return "Profile{" +
            "name='" + name + '\'' +
            ", fractions=" + fractions +
            '}';
    }
}
