package org.bitbucket.vvv1559.powerhouseproblem.dao.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.bitbucket.vvv1559.powerhouseproblem.Utils;

import java.util.Map;
import java.util.Objects;

public class MeterData {
    private final int connectionId;
    private final String profile;
    @JsonSerialize
    private Map<Month, Integer> meterReadings = Utils.initMonthMap();

    public MeterData(int connectionId, String profile) {
        this.connectionId = connectionId;
        this.profile = profile;
    }

    public int getConnectionId() {
        return connectionId;
    }

    public String getProfile() {
        return profile;
    }

    public void setReading(Month month, int reading) {
        meterReadings.put(month, reading);
    }

    public int getReading(Month month) {
        return meterReadings.get(month);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MeterData meterData = (MeterData) o;
        return connectionId == meterData.connectionId &&
            Objects.equals(profile, meterData.profile) &&
            Objects.equals(meterReadings, meterData.meterReadings);
    }

    @Override
    public int hashCode() {
        return Objects.hash(connectionId, profile, meterReadings);
    }

    @Override
    public String toString() {
        return "MeterData{" +
            "connectionId=" + connectionId +
            ", profile='" + profile + '\'' +
            ", meterReadings=" + meterReadings +
            '}';
    }
}
