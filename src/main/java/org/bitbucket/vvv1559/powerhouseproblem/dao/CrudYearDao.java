package org.bitbucket.vvv1559.powerhouseproblem.dao;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class CrudYearDao<K, V> {
    private final Map<K, V> dataMap = new HashMap<>();

    public void save(K key, V data, boolean isNew) {
        if (isNew) {
            dataMap.put(key, data);
        } else if (dataMap.containsKey(key)) {
            dataMap.put(key, data);
        }
    }

    public V getData(K key) {
        return dataMap.get(key);
    }

    public void deleteData(K key) {
        dataMap.remove(key);
    }

    public Collection<V> getAllData() {
        return dataMap.values();
    }
}
