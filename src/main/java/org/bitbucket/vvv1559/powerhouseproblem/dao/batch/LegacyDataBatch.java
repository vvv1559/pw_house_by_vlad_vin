package org.bitbucket.vvv1559.powerhouseproblem.dao.batch;

import org.bitbucket.vvv1559.powerhouseproblem.dao.validation.ValidationResult;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class LegacyDataBatch<K, V> {
    private final Map<K, V> batch = new HashMap<>();

    public V computeIfAbsent(K key, Function<K, V> valueGenerator) {
        return batch.computeIfAbsent(key, valueGenerator);
    }

    public ValidationResult confirmBatch(Function<V, ValidationResult> batchProcessor) {
        if (batch.isEmpty()) {
            throw new IllegalStateException("Batch is empty");
        }
        ValidationResult validationResult = new ValidationResult();
        batch.values().stream().map(batchProcessor).forEach(validationResult::unionResult);

        batch.clear();
        return validationResult;
    }
}
