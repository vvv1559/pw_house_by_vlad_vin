package org.bitbucket.vvv1559.powerhouseproblem.dao.validation;

import java.util.ArrayList;
import java.util.List;

public class ValidationResult {
    public static final ValidationResult OK = new ValidationResult();
    private final List<String> validationErrors = new ArrayList<>();

    public ValidationResult() {
    }

    public ValidationResult(String error) {
        addError(error);
    }

    public ValidationResult(String format, Object... args) {
        addError(format, args);
    }

    public void addError(String format, Object... args) {
        validationErrors.add(String.format(format, args));
    }

    public void addError(String error) {
        validationErrors.add(error);
    }

    public void unionResult(ValidationResult validationResult) {
        validationErrors.addAll(validationResult.validationErrors);
    }

    public boolean isCorrect() {
        return validationErrors.isEmpty();
    }

    public String[] getErrors() {
        return validationErrors.toArray(new String[validationErrors.size()]);
    }
}
