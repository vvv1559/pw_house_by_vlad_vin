package org.bitbucket.vvv1559.powerhouseproblem.dao.validation;

public class ValidationException extends RuntimeException {
    public ValidationException(String message) {
        super(message);
    }

    public ValidationException(ValidationResult validationResult) {
        super(String.join("\n", validationResult.getErrors()));
    }
}
