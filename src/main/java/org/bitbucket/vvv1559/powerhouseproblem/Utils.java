package org.bitbucket.vvv1559.powerhouseproblem;

import org.bitbucket.vvv1559.powerhouseproblem.dao.model.Month;

import java.util.HashMap;
import java.util.Map;

public class Utils {
    private Utils() {
    }

    public static  <T> Map<Month, T> initMonthMap(){
        Map<Month, T> result = new HashMap<>();
        for (Month month : Month.values()) {
            result.put(month, null);
        }

        return result;
    }
}
