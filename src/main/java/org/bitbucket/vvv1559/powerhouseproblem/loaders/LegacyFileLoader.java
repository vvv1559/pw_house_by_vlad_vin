package org.bitbucket.vvv1559.powerhouseproblem.loaders;

import com.google.common.annotations.VisibleForTesting;
import org.apache.log4j.Logger;
import org.bitbucket.vvv1559.powerhouseproblem.dao.CrudYearDao;
import org.bitbucket.vvv1559.powerhouseproblem.dao.batch.LegacyDataBatch;
import org.bitbucket.vvv1559.powerhouseproblem.dao.model.MeterData;
import org.bitbucket.vvv1559.powerhouseproblem.dao.model.Month;
import org.bitbucket.vvv1559.powerhouseproblem.dao.model.Profile;
import org.bitbucket.vvv1559.powerhouseproblem.dao.validation.ValidationException;
import org.bitbucket.vvv1559.powerhouseproblem.dao.validation.ValidationResult;
import org.bitbucket.vvv1559.powerhouseproblem.dao.validation.ValidationService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

@Component
public class LegacyFileLoader {

    private static final String CSV_SEPARATOR = ",";
    private static final String ERRORS_FILE_POSTFIX = "_errors";

    private enum FileType {
        PROFILE,
        METER_DATA
    }

    private static final Logger log = Logger.getLogger(LegacyFileLoader.class);

    @Value("${file-loader.path:.}")
    private String filesPath;

    @Value("${file-loader.interval-minutes}")
    private int intervalMinutes;

    @Value("${file-loader.names-prefix.fractions}")
    private String profileFileNamePrefix;

    @Value("${file-loader.names-prefix.meterData}")
    private String dataFileNamePrefix;

    private final CrudYearDao<String, Profile> profilesDao;
    private final CrudYearDao<Integer, MeterData> meterDataDao;
    private final ValidationService validationService;


    private final Set<File> filesInProcess = new HashSet<>();

    public LegacyFileLoader(CrudYearDao<String, Profile> profilesDao, CrudYearDao<Integer, MeterData> meterDataDao,
                            ValidationService validationService) {
        this.profilesDao = profilesDao;
        this.meterDataDao = meterDataDao;
        this.validationService = validationService;
    }

    public void start() {
        new Thread(() -> {
            log.info("Start file watcher");
            while (!Thread.interrupted()) {
                try {
                    loadDataFromDisk();
                    log.info("Go to sleep on " + intervalMinutes + " minutes");
                    TimeUnit.MINUTES.sleep(intervalMinutes);
                } catch (Exception e) {
                    log.error("Error during files processing", e);
                }
            }
        }).start();
    }

    private void loadDataFromDisk() throws IOException {

        File path = new File(filesPath);
        if (!path.exists()) {
            log.error("Path " + path + " does not exists");
        }

        File[] filesToRead = path.listFiles();
        if (filesToRead == null) {
            log.info("Nothing to read");
            return;
        }

        for (File file : filesToRead) {
            File errorsFile = buildErrorsFilePath(file);

            if (file.isDirectory() || filesInProcess.contains(file) || errorsFile.exists()) {
                continue;
            }
            try {
                log.info("Start file reading " + file);
                filesInProcess.add(file);
                if (file.getName().startsWith(profileFileNamePrefix)) {
                    loadFile(file, FileType.PROFILE);
                } else if (file.getName().startsWith(dataFileNamePrefix)) {
                    loadFile(file, FileType.METER_DATA);
                } else {
                    log.info(file + "Not a power house file. Skip");
                }
            } catch (Exception e) {
                log.warn("Some errors during process file " + file + " Check file " + errorsFile);
                try (BufferedWriter bw = new BufferedWriter(new FileWriter(errorsFile))) {
                    bw.append(e.getMessage());
                }
            } finally {
                filesInProcess.remove(file);
            }
        }

    }

    private File buildErrorsFilePath(File file) {
        return file.toPath().getParent().resolve(file.getName() + ERRORS_FILE_POSTFIX).toFile();
    }

    private void loadFile(File file, FileType fileType) throws IOException {
        ValidationResult validationResult = new ValidationResult();
        List<String> fileRows = Files.readAllLines(file.toPath(), Charset.defaultCharset());
        switch (fileType) {
            case PROFILE:
                LegacyDataBatch<String, Profile> profileBatch = rowsToProfileBatch(fileRows);
                validationResult = profileBatch.confirmBatch(profile -> validateAndSave(profilesDao, profile, validationService::validateProfile, Profile::getName));
                break;
            case METER_DATA:
                LegacyDataBatch<Integer, MeterData> meterDataBatch = rowsToMeterDataBatch(fileRows);
                validationResult = meterDataBatch.confirmBatch(meterData -> validateAndSave(meterDataDao, meterData, validationService::validateMeterData, MeterData::getConnectionId));
                break;
        }

        if (validationResult.isCorrect()) {
            log.info(file + " loaded. Remove file");
            //noinspection ResultOfMethodCallIgnored
            file.delete();
        } else {
            throw new ValidationException(validationResult);
        }
    }


    /**
     * Convert rows in legacy format into batch.
     * Legacy profile format is "Month,Profile,Fraction" For example: JAN,A,0.2
     *
     * @param rows List of rows
     * @return batch
     */
    @VisibleForTesting
    LegacyDataBatch<String, Profile> rowsToProfileBatch(List<String> rows) {
        LegacyDataBatch<String, Profile> batch = new LegacyDataBatch<>();
        for (String row : rows) {
            String[] rowSplit = row.split(CSV_SEPARATOR);
            Month month = Month.valueOf(rowSplit[0]);
            String profileName = rowSplit[1];
            double fraction = Double.parseDouble(rowSplit[2]);
            Profile profile = batch.computeIfAbsent(profileName, Profile::new);
            profile.setFraction(month, fraction);
        }

        return batch;
    }

    /**
     * Convert rows in legacy format into batch.
     * Legacy meter data format is "ConnectionID,Profile,Month,Meter reading" For example: 0001,A,JAN,10
     *
     * @param rows List of rows
     * @return batch
     */
    @VisibleForTesting
    LegacyDataBatch<Integer, MeterData> rowsToMeterDataBatch(List<String> rows) {
        LegacyDataBatch<Integer, MeterData> batch = new LegacyDataBatch<>();
        for (String row : rows) {
            String[] rowSplit = row.split(CSV_SEPARATOR);
            Integer connectionId = Integer.parseInt(rowSplit[0]);
            String profile = rowSplit[1];
            Month month = Month.valueOf(rowSplit[2]);
            Integer meterValue = Integer.parseInt(rowSplit[3]);
            MeterData meterData = batch.computeIfAbsent(connectionId, k -> new MeterData(connectionId, profile));
            meterData.setReading(month, meterValue);
        }

        return batch;
    }

    private static <K, V> ValidationResult validateAndSave(CrudYearDao<K, V> crudYearDao, V data,
                                                           Function<V, ValidationResult> validator,
                                                           Function<V, K> keyExtractor) {
        ValidationResult validationResult = validator.apply(data);

        if (validationResult.isCorrect()) {
            crudYearDao.save(keyExtractor.apply(data), data, true);
        }
        return validationResult;
    }
}
