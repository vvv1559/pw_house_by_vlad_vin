package org.bitbucket.vvv1559.powerhouseproblem.controllers;

import org.bitbucket.vvv1559.powerhouseproblem.dao.CrudYearDao;
import org.bitbucket.vvv1559.powerhouseproblem.dao.model.MeterData;
import org.bitbucket.vvv1559.powerhouseproblem.dao.model.Month;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/consumption")
public class ConsumptionController {

    private final CrudYearDao<Integer, MeterData> meterDataDao;

    @Autowired
    public ConsumptionController(CrudYearDao<Integer, MeterData> meterDataDao) {
        this.meterDataDao = meterDataDao;
    }


    @GetMapping
    public int getConsumption(@RequestParam Month month) {
        int totalConsumption = 0;
        for (MeterData meterData : meterDataDao.getAllData()) {
            totalConsumption += meterData.getReading(month);
        }
        return totalConsumption;
    }
}
