package org.bitbucket.vvv1559.powerhouseproblem.controllers;

import org.bitbucket.vvv1559.powerhouseproblem.dao.CrudYearDao;
import org.bitbucket.vvv1559.powerhouseproblem.dao.batch.LegacyDataBatch;
import org.bitbucket.vvv1559.powerhouseproblem.dao.model.MeterData;
import org.bitbucket.vvv1559.powerhouseproblem.dao.model.Month;
import org.bitbucket.vvv1559.powerhouseproblem.dao.validation.ValidationResult;
import org.bitbucket.vvv1559.powerhouseproblem.dao.validation.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/meterData")
public class MeterDataController {

    private final CrudYearDao<Integer, MeterData> meterDataDao;
    private final ValidationService validationService;

    private final LegacyDataBatch<Integer, MeterData> batch = new LegacyDataBatch<>();

    @Autowired
    public MeterDataController(CrudYearDao<Integer, MeterData> meterDataDao, ValidationService validationService) {
        this.meterDataDao = meterDataDao;
        this.validationService = validationService;
    }

    @PostMapping("/addToBatch")
    public void addToBatch(@RequestParam int connectionId,
                           @RequestParam String profile,
                           @RequestParam Month month,
                           @RequestParam int meterValue) {
        batch.computeIfAbsent(connectionId, k -> new MeterData(connectionId, profile)).setReading(month, meterValue);
    }

    @PostMapping("/confirmBatch")
    public ResponseEntity<ValidationResult> confirmBatch() {
        return ResponseEntity.ok().body(batch.confirmBatch(meterData -> validateAndSave(meterData, true)));
    }


    @PostMapping
    public ResponseEntity<ValidationResult> addMeterData(@RequestBody MeterData meterData) {
        return ResponseEntity.ok().body(validateAndSave(meterData, true));

    }

    @GetMapping
    public ResponseEntity<MeterData> getMeterDataForConnection(@RequestParam Integer connectionId) {
        return ResponseEntity.ok(meterDataDao.getData(connectionId));
    }

    @PutMapping
    public ResponseEntity<ValidationResult> updateMeterData(@RequestParam MeterData meterData) {
        return ResponseEntity.ok().body(validateAndSave(meterData, false));
    }

    @DeleteMapping
    public void deleteMeterDataForConnection(@RequestParam Integer connectionId) {
        meterDataDao.deleteData(connectionId);
    }

    private ValidationResult validateAndSave(MeterData meterData, boolean isNew) {
        ValidationResult validationResult = validationService.validateMeterData(meterData);
        if (validationResult.isCorrect()) {
            meterDataDao.save(meterData.getConnectionId(), meterData, isNew);
        }
        return validationResult;
    }
}
