package org.bitbucket.vvv1559.powerhouseproblem.controllers;

import org.bitbucket.vvv1559.powerhouseproblem.dao.CrudYearDao;
import org.bitbucket.vvv1559.powerhouseproblem.dao.batch.LegacyDataBatch;
import org.bitbucket.vvv1559.powerhouseproblem.dao.model.Month;
import org.bitbucket.vvv1559.powerhouseproblem.dao.model.Profile;
import org.bitbucket.vvv1559.powerhouseproblem.dao.validation.ValidationResult;
import org.bitbucket.vvv1559.powerhouseproblem.dao.validation.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/profile")
public class ProfileController {

    private final CrudYearDao<String, Profile> profileDao;
    private final ValidationService validationService;

    private final LegacyDataBatch<String, Profile> batch = new LegacyDataBatch<>();

    @Autowired
    public ProfileController(CrudYearDao<String, Profile> profileDao, ValidationService validationService) {
        this.profileDao = profileDao;
        this.validationService = validationService;
    }

    @PostMapping("/addToBatch")
    public void addToBatch(@RequestParam String profileName, @RequestParam Month month, @RequestParam double fraction) {
        batch.computeIfAbsent(profileName, k -> new Profile(profileName)).setFraction(month, fraction);
    }

    @PostMapping("/confirmBatch")
    public ResponseEntity<ValidationResult> confirmBatch() {
        return ResponseEntity.ok().body(batch.confirmBatch(profile -> validateAndSave(profile, true)));
    }


    @PostMapping
    public ResponseEntity<ValidationResult> addProfile(@RequestBody Profile profile) {
        return ResponseEntity.ok().body(validateAndSave(profile, true));
    }

    @GetMapping
    public ResponseEntity<Profile> getProfile(@RequestParam String profileName) {
        return ResponseEntity.ok(profileDao.getData(profileName));
    }

    @PutMapping
    public ResponseEntity<ValidationResult> updateProfile(@RequestParam Profile profile) {
        return ResponseEntity.ok().body(validateAndSave(profile, false));
    }

    @DeleteMapping
    public void deleteProfile(@RequestParam String profile) {
        profileDao.deleteData(profile);
    }

    private ValidationResult validateAndSave(Profile profile, boolean isNew) {
        ValidationResult validationResult = validationService.validateProfile(profile);
        if (validationResult.isCorrect()) {
            profileDao.save(profile.getName(), profile, isNew);
        }
        return validationResult;
    }
}
