package org.bitbucket.vvv1559.powerhouseproblem.loaders;

import org.bitbucket.vvv1559.powerhouseproblem.TestUtils;
import org.bitbucket.vvv1559.powerhouseproblem.dao.batch.LegacyDataBatch;
import org.bitbucket.vvv1559.powerhouseproblem.dao.model.MeterData;
import org.bitbucket.vvv1559.powerhouseproblem.dao.model.Profile;
import org.bitbucket.vvv1559.powerhouseproblem.dao.validation.ValidationResult;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Stream;

public class LegacyFileLoaderTest {

    private LegacyFileLoader legacyFileLoader = new LegacyFileLoader(null, null, null);

    @Test
    public void rowsToProfileBatch() throws Exception {
        Profile validProfile = TestUtils.buildValidProfile("Valid profile");
        Profile invalidProfile = TestUtils.buildInvalidProfile("Invalid profile");
        validateLoader(TestUtils::toLegacyDataFormat, legacyFileLoader::rowsToProfileBatch, validProfile, invalidProfile);
    }

    @Test
    public void rowsToMeterDataBatch() throws Exception {
        Profile profile = TestUtils.buildValidProfile("Valid profile");
        MeterData meterData1 = TestUtils.buildValidMeterData(1, profile);
        MeterData meterData2 = TestUtils.buildValidMeterData(2, profile);
        validateLoader(TestUtils::toLegacyDataFormat, legacyFileLoader::rowsToMeterDataBatch, meterData1, meterData2);
    }

    @SafeVarargs
    private final <T> void validateLoader(Function<T, List<String>> legacyConverter,
                                          Function<List<String>, LegacyDataBatch<?, T>> toBatchConverter,
                                          T... entities) {
        List<String> data = new ArrayList<>();
        for (T entity : entities) {
            data.addAll(legacyConverter.apply(entity));
        }
        Collections.shuffle(data);

        LegacyDataBatch<?, T> dataBatch = toBatchConverter.apply(data);
        AtomicInteger executionCounter = new AtomicInteger();

        dataBatch.confirmBatch(dataEntity -> {
            if (Stream.of(entities).noneMatch(dataEntity::equals)) {
                Assert.fail();
            }

            executionCounter.incrementAndGet();
            return new ValidationResult();
        });

        Assert.assertEquals(entities.length, executionCounter.get());
    }

}