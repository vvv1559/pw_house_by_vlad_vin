package org.bitbucket.vvv1559.powerhouseproblem.dao.batch;

import org.bitbucket.vvv1559.powerhouseproblem.TestUtils;
import org.bitbucket.vvv1559.powerhouseproblem.dao.model.Profile;
import org.bitbucket.vvv1559.powerhouseproblem.dao.validation.ValidationResult;
import org.junit.Assert;
import org.junit.Test;

public class LegacyDataBatchTest {
    @Test
    public void testBatch() throws Exception {
        LegacyDataBatch<String, Profile> profileBatch = new LegacyDataBatch<>();
        Profile profile = TestUtils.buildValidProfile("Some Valid profile");
        Profile invalidProfile = TestUtils.buildInvalidProfile("Some Invalid profile");
        profileBatch.computeIfAbsent(invalidProfile.getName(), k -> profile);
        profileBatch.computeIfAbsent(invalidProfile.getName(), k -> invalidProfile);

        profileBatch.confirmBatch(p -> {
            Assert.assertEquals(p, profile);
            Assert.assertNotEquals(p, invalidProfile);
            return new ValidationResult();
        });
    }

    @Test(expected = IllegalStateException.class)
    public void emptyBatchRaiseError() {
        new LegacyDataBatch<>().confirmBatch(null);
    }
}