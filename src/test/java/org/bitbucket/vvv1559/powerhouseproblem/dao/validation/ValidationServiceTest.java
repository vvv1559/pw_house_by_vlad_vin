package org.bitbucket.vvv1559.powerhouseproblem.dao.validation;

import org.bitbucket.vvv1559.powerhouseproblem.TestUtils;
import org.bitbucket.vvv1559.powerhouseproblem.dao.CrudYearDao;
import org.bitbucket.vvv1559.powerhouseproblem.dao.model.Profile;
import org.junit.Assert;
import org.junit.Test;

import java.util.function.Function;

public class ValidationServiceTest {
    private CrudYearDao<String, Profile> profileDao = new CrudYearDao<>();
    private ValidationService validationService = new ValidationService(profileDao);

    private <T> void validate(T entity, Function<T, ValidationResult> validator, boolean isValid) {
        ValidationResult result = validator.apply(entity);
        Assert.assertEquals("Entity must be " + isValid + " " + entity.toString(), isValid, result.isCorrect());
    }

    @Test
    public void validateProfile() throws Exception {
        validate(TestUtils.buildValidProfile("Valid profile"), validationService::validateProfile, true);
        validate(TestUtils.buildInvalidProfile("Invalid profile"), validationService::validateProfile, false);
    }

    @Test
    public void validateMeterData() throws Exception {
        Profile profile = TestUtils.buildValidProfile("Valid profile");
        profileDao.save(profile.getName(), profile, true);

        validate(TestUtils.buildValidMeterData(1, profile), validationService::validateMeterData, true);
        validate(TestUtils.buildInvalidMeterDataWithNegativeConsumption(2, profile), validationService::validateMeterData, false);
        validate(TestUtils.buildInvalidMeterDataWithOverConsumption(3, profile), validationService::validateMeterData, false);

    }

}