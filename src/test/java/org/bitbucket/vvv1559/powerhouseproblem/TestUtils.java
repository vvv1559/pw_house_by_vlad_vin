package org.bitbucket.vvv1559.powerhouseproblem;

import org.bitbucket.vvv1559.powerhouseproblem.dao.model.MeterData;
import org.bitbucket.vvv1559.powerhouseproblem.dao.model.Month;
import org.bitbucket.vvv1559.powerhouseproblem.dao.model.Profile;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public class TestUtils {
    public static final int TOTAL_CONSUMPTION = 240;
    public static final String VALID_PROFILE_NAME = "Valid profile";

    public static Profile buildValidProfile(String name) {
        Profile validProfile = new Profile(name);
        validProfile.setFraction(Month.JAN, 0.15);
        validProfile.setFraction(Month.FEB, 0.20);
        validProfile.setFraction(Month.MAR, 0.15);
        validProfile.setFraction(Month.APR, 0.10);
        validProfile.setFraction(Month.MAY, 0.05);
        validProfile.setFraction(Month.JUN, 0.00);
        validProfile.setFraction(Month.JUL, 0.00);
        validProfile.setFraction(Month.AUG, 0.00);
        validProfile.setFraction(Month.SEP, 0.05);
        validProfile.setFraction(Month.OCT, 0.05);
        validProfile.setFraction(Month.NOV, 0.1);
        validProfile.setFraction(Month.DEC, 0.15);

        return validProfile;
    }

    public static Profile buildInvalidProfile(String name) {
        Profile invalidProfile = new Profile(name);
        for (Month month : Month.values()) {
            invalidProfile.setFraction(month, 0.1);
        }
        return invalidProfile;
    }

    public static MeterData buildValidMeterData(int connectionId, Profile profile) {
        return buildMeterData(connectionId, profile);
    }

    public static MeterData buildInvalidMeterDataWithNegativeConsumption(int connectionId, Profile profile) {
        MeterData meterData = buildMeterData(connectionId, profile);
        meterData.setReading(Month.JUL, meterData.getReading(Month.JUN) - 10);
        return meterData;
    }

    public static MeterData buildInvalidMeterDataWithOverConsumption(int connectionId, Profile profile) {
        MeterData meterData = buildMeterData(connectionId, profile);
        meterData.setReading(Month.JUL, meterData.getReading(Month.JUN) * 2);
        return meterData;
    }

    private static MeterData buildMeterData(int connectionId, Profile profile) {

        MeterData meterData = new MeterData(connectionId, profile.getName());

        Month[] months = Month.values();
        for (Month month : months) {
            int consumption = (int) (profile.getFraction(month) * TOTAL_CONSUMPTION);

            if (month == Month.JAN) {
                meterData.setReading(month, consumption);
            } else {
                Month prevMonth = months[month.ordinal() - 1];
                meterData.setReading(month, meterData.getReading(prevMonth) + consumption);
            }
        }
        return meterData;
    }

    //"Month,Profile,Fraction" For example: JAN,A,0.2
    public static List<String> toLegacyDataFormat(Profile profile) {
        List<String> result = new ArrayList<>();

        DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
        decimalFormatSymbols.setDecimalSeparator('.');
        NumberFormat formatter = new DecimalFormat("#0.00", decimalFormatSymbols);
        for (Month month : Month.values()) {
            result.add(String.join(",",
                month.name(), profile.getName(), formatter.format(profile.getFraction(month))));
        }

        return result;
    }

    //"ConnectionID,Profile,Month,Meter reading" For example: 0001,A,JAN,10
    public static List<String> toLegacyDataFormat(MeterData meterData) {
        List<String> result = new ArrayList<>();
        for (Month month : Month.values()) {
            result.add(String.join(",",
                String.valueOf(meterData.getConnectionId()), meterData.getProfile(),
                month.name(), String.valueOf(meterData.getReading(month))));
        }

        return result;
    }

}
