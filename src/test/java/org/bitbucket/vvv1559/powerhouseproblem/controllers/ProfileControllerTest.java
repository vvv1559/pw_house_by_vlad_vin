package org.bitbucket.vvv1559.powerhouseproblem.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.bitbucket.vvv1559.powerhouseproblem.TestUtils;
import org.bitbucket.vvv1559.powerhouseproblem.dao.CrudYearDao;
import org.bitbucket.vvv1559.powerhouseproblem.dao.model.Profile;
import org.bitbucket.vvv1559.powerhouseproblem.dao.validation.ValidationResult;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ProfileControllerTest {

    @Autowired
    WebApplicationContext wac;

    @Autowired
    private CrudYearDao<String, Profile> profilesDao;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).dispatchOptions(true).build();
    }

    @After
    public void cleanup() {
        profilesDao.deleteData(TestUtils.VALID_PROFILE_NAME);
    }

    private void checkNotExistAndAddToBatch(Profile profile) throws Exception {
        // Check that profile does not exist
        mockMvc.perform(get("/profile").param("profileName", profile.getName()))
            .andExpect(status().isOk())
            .andExpect(content().string(""));

        List<String> legacyFormattedData = TestUtils.toLegacyDataFormat(profile);
        Collections.shuffle(legacyFormattedData);
        for (String row : legacyFormattedData) {
            String[] rowSplits = row.split(",");

            MockHttpServletRequestBuilder addRequest = post("/profile/addToBatch")
                .param("month", rowSplits[0])
                .param("profileName", rowSplits[1])
                .param("fraction", rowSplits[2]);

            mockMvc.perform(addRequest)
                .andExpect(status().isOk())
                .andExpect(content().string(""));
        }
    }

    @Test
    public void addCorrectBatchTest() throws Exception {
        Profile profile = TestUtils.buildValidProfile(TestUtils.VALID_PROFILE_NAME);
        checkNotExistAndAddToBatch(profile);

        ObjectMapper objectMapper = new ObjectMapper();
        String okValidationJson = objectMapper.writeValueAsString(ValidationResult.OK);
        mockMvc.perform(post("/profile/confirmBatch"))
            .andExpect(status().isOk())
            .andExpect(content().json(okValidationJson));

        String profileJson = objectMapper.writeValueAsString(profile);
        mockMvc.perform(get("/profile").param("profileName", profile.getName()))
            .andExpect(status().isOk())
            .andExpect(content().json(profileJson))
            .andExpect(jsonPath("fractions", notNullValue()));

    }

    @Test
    public void addIncorrectBatchTest() throws Exception {
        Profile profile = TestUtils.buildInvalidProfile("Invalid profile");
        checkNotExistAndAddToBatch(profile);

        mockMvc.perform(post("/profile/confirmBatch"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("correct", is(false)));

        // Check that incorrect profile not saved
        mockMvc.perform(get("/profile").param("profileName", profile.getName()))
            .andExpect(status().isOk())
            .andExpect(content().string(""));
    }
}