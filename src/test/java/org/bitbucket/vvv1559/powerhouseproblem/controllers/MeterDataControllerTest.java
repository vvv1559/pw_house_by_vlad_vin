package org.bitbucket.vvv1559.powerhouseproblem.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.bitbucket.vvv1559.powerhouseproblem.TestUtils;
import org.bitbucket.vvv1559.powerhouseproblem.dao.CrudYearDao;
import org.bitbucket.vvv1559.powerhouseproblem.dao.model.MeterData;
import org.bitbucket.vvv1559.powerhouseproblem.dao.model.Profile;
import org.bitbucket.vvv1559.powerhouseproblem.dao.validation.ValidationResult;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
@RunWith(SpringRunner.class)
public class MeterDataControllerTest {
    private static final int CORRECT_DATA_CONNECTION_ID = 1;

    @Autowired
    WebApplicationContext wac;

    @Autowired
    private CrudYearDao<String, Profile> profilesDao;

    @Autowired
    private CrudYearDao<Integer, MeterData> meterDataDao;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).dispatchOptions(true).build();
    }

    @After
    public void cleanup() {
        profilesDao.deleteData(TestUtils.VALID_PROFILE_NAME);
        meterDataDao.deleteData(CORRECT_DATA_CONNECTION_ID);
    }

    private void checkNotExistAndAddToBatch(MeterData meterData) throws Exception {
        // Check that profile does not exist
        mockMvc.perform(get("/meterData").param("connectionId", String.valueOf(meterData.getConnectionId())))
            .andExpect(status().isOk())
            .andExpect(content().string(""));

        List<String> legacyFormattedData = TestUtils.toLegacyDataFormat(meterData);
        Collections.shuffle(legacyFormattedData);
        for (String row : legacyFormattedData) {
            String[] rowSplits = row.split(",");

            MockHttpServletRequestBuilder addRequest = post("/meterData/addToBatch")
                .param("connectionId", rowSplits[0])
                .param("profile", rowSplits[1])
                .param("month", rowSplits[2])
                .param("meterValue", rowSplits[3]);

            mockMvc.perform(addRequest)
                .andExpect(status().isOk())
                .andExpect(content().string(""));
        }
    }

    @Test
    public void addCorrectBatchTest() throws Exception {
        Profile validProfile = TestUtils.buildValidProfile(TestUtils.VALID_PROFILE_NAME);
        profilesDao.save(validProfile.getName(), validProfile, true);

        MeterData meterData = TestUtils.buildValidMeterData(CORRECT_DATA_CONNECTION_ID, validProfile);
        checkNotExistAndAddToBatch(meterData);

        ObjectMapper objectMapper = new ObjectMapper();
        String okValidationJson = objectMapper.writeValueAsString(ValidationResult.OK);
        mockMvc.perform(post("/meterData/confirmBatch"))
            .andExpect(status().isOk())
            .andExpect(content().json(okValidationJson));

        String meterDataJson = objectMapper.writeValueAsString(meterData);
        mockMvc.perform(get("/meterData").param("connectionId", String.valueOf(meterData.getConnectionId())))
            .andExpect(status().isOk())
            .andExpect(content().json(meterDataJson))
            .andExpect(jsonPath("meterReadings", notNullValue()));

    }

    private void checkInvalidData(MeterData meterData) throws Exception {
        checkNotExistAndAddToBatch(meterData);
        mockMvc.perform(post("/meterData/confirmBatch"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("correct", is(false)));

        // Check that incorrect data not saved
        mockMvc.perform(get("/meterData").param("connectionId", String.valueOf(meterData.getConnectionId())))
            .andExpect(status().isOk())
            .andExpect(content().string(""));
    }

    @Test
    public void addIncorrectBatchTest() throws Exception {
        Profile validProfile = TestUtils.buildValidProfile(TestUtils.VALID_PROFILE_NAME);
        profilesDao.save(validProfile.getName(), validProfile, false);

        checkInvalidData(TestUtils.buildInvalidMeterDataWithNegativeConsumption(2, validProfile));
        checkInvalidData(TestUtils.buildInvalidMeterDataWithOverConsumption(3, validProfile));

        MeterData meterData = TestUtils.buildValidMeterData(4, validProfile);
        ReflectionTestUtils.setField(meterData, "profile", "Unknown profile");
        checkInvalidData(meterData);
    }

}