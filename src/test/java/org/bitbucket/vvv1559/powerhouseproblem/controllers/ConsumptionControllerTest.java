package org.bitbucket.vvv1559.powerhouseproblem.controllers;

import org.bitbucket.vvv1559.powerhouseproblem.TestUtils;
import org.bitbucket.vvv1559.powerhouseproblem.dao.CrudYearDao;
import org.bitbucket.vvv1559.powerhouseproblem.dao.model.MeterData;
import org.bitbucket.vvv1559.powerhouseproblem.dao.model.Month;
import org.bitbucket.vvv1559.powerhouseproblem.dao.model.Profile;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ConsumptionControllerTest {
    private static final int[] dataIndexes = new int[]{1, 2};

    @Autowired
    WebApplicationContext wac;

    private MockMvc mockMvc;

    @Autowired
    private CrudYearDao<Integer, MeterData> meterDataDao;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).dispatchOptions(true).build();
    }

    @After
    public void cleanup() {
        Arrays.stream(dataIndexes).forEach(meterDataDao::deleteData);
    }

    @Test
    public void getConsumption() throws Exception {
        Profile validProfile = TestUtils.buildValidProfile("Valid profile");
        MeterData meterData1 = TestUtils.buildValidMeterData(dataIndexes[0], validProfile);
        meterDataDao.save(meterData1.getConnectionId(), meterData1, true);

        mockMvc.perform(get("/consumption").param("month", Month.DEC.name()))
            .andExpect(status().isOk())
            .andExpect(content().string(String.valueOf(TestUtils.TOTAL_CONSUMPTION)));

        MeterData meterData2 = TestUtils.buildValidMeterData(dataIndexes[1], validProfile);
        meterDataDao.save(meterData2.getConnectionId(), meterData2, true);

        mockMvc.perform(get("/consumption").param("month", Month.DEC.name()))
            .andExpect(status().isOk())
            .andExpect(content().string(String.valueOf(2 * TestUtils.TOTAL_CONSUMPTION)));

    }

}